//
//  TitlesDetailViewController.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 01/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation
import UIKit

class TitlesDetailViewController: UIViewController {
    
    @IBOutlet weak var imageThumbnail: UIImageView!
    @IBOutlet weak var imageURL: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var titles:SampleServices?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageThumbnail.layer.borderWidth = 1
        imageThumbnail.layer.masksToBounds = false
        imageThumbnail.layer.borderColor = UIColor.gray.cgColor
        imageThumbnail.layer.cornerRadius = imageThumbnail.frame.height/2
        imageThumbnail.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.displayTitlesDetails()
    }
    
    func displayTitlesDetails()
    {
        
        if let url = NSURL(string: (self.titles?.thumbnailUrl)!){
            if let data = NSData(contentsOf: url as URL){
                imageThumbnail.contentMode = UIViewContentMode.scaleAspectFit
                imageThumbnail.image = UIImage(data: data as Data)
            }
        }
        if let url = NSURL(string: (self.titles?.url)!){
            if let data = NSData(contentsOf: url as URL){
                imageURL.contentMode = UIViewContentMode.scaleAspectFit
                imageURL.image = UIImage(data: data as Data)
            }
        }
        
        self.label!.text = self.titles?.url
//
//        self.descriptionLabel!.sizeToFit() // to top-align text
    }
    
}
