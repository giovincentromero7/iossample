//
//  SearchController.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 02/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation
import Alamofire

enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}

class ImageSearchResult {
    let imageURL:String?
    let source:String?
    let attributionURL:String?
    var image:UIImage?
    
    required init(anImageURL: String?, aSource: String?, anAttributionURL: String?) {
        imageURL = anImageURL
        source = aSource
        attributionURL = anAttributionURL
    }
    
    func fullAttribution() -> String {
        var result:String = ""
        if attributionURL != nil && attributionURL!.isEmpty == false {
            result += "Image from \(attributionURL!)"
        }
        if source != nil && source!.isEmpty == false  {
            if result.isEmpty {
                result += "Image from "
            }
            result += " \(source!)"
        }
        return result
    }
}

class SearchController {
    static let IMAGE_KEY = "Image"
    static let SOURCE_KEY = "AbstractSource"
    static let ATTRIBUTION_KEY = "AbstractURL"
    
    fileprivate class func endpointForSearchString(_ searchString: String) -> String {
        
        return ""
    }
    
    class func imageFromSearchString(_ searchString: String, completionHandler: @escaping (Result<ImageSearchResult>) -> Void) {
        let searchURLString = endpointForSearchString(searchString)
        Alamofire.request(searchURLString)
            .responseJSON { response in
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                
                let imageURLResult = imageFromResponse(response)
                
                guard imageURLResult.isSuccess,
                    let result = imageURLResult.value else {
                        completionHandler(.failure(imageURLResult.error!))
                        return
                }
                
                guard let imageURL = result.imageURL,
                    !imageURL.isEmpty else {
                        completionHandler(.failure(BackendError.objectSerialization(reason:
                            "Could not get image URL from JSON")))
                        return
                }
                
                // got the URL, now to load it
                Alamofire.request(imageURL)
                    .response { response in
                        if response.data == nil {
                            completionHandler(.failure(BackendError.objectSerialization(reason:
                                "Could not get image data from URL")))
                            return
                        }
                        result.image = UIImage(data: response.data!)
                        completionHandler(.success(result))
                }
        }
    }
    
    private class func imageFromResponse(_ response: DataResponse<Any>) -> Result<ImageSearchResult> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }
        
        // make sure we got JSON and it's a dictionary
        guard let json = response.result.value as? [String: Any] else {
            print("didn't get image info as JSON from API")
            return .failure(BackendError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }
        
        // turn JSON in to Image object
        guard let imageURL = json[IMAGE_KEY] as? String,
            let source = json[SOURCE_KEY] as? String,
            let attribution = json[ATTRIBUTION_KEY] as? String else {
                return .failure(BackendError.objectSerialization(reason:
                    "Could not get image from JSON"))
        }
        
        let result = ImageSearchResult(anImageURL: imageURL, aSource: source, anAttributionURL: attribution)
        return .success(result)
    }
}
