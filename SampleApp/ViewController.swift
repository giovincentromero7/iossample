//
//  ViewController.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 31/05/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    var titles: [SampleServices]?
    var titlesWrapper: SampleWrapper? // holds the last wrapper that we've loaded
    var isLoadingTitles = false
    var imageCache: Dictionary<String, ImageSearchResult?>?
    
    var titlesSearchResults: [SampleServices]?
    
    @IBOutlet weak var tableview: UITableView?
    var searchController: UISearchController = UISearchController(searchResultsController: nil)
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
//        searchController.searchBar.scopeButtonTitles = ["Title", "Id"]
        
        tableview?.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        imageCache = Dictionary<String, ImageSearchResult>()
        
        self.loadFirstTitles()
    }
    
    // MARK: Loading Titles from API
    
    func loadFirstTitles() {
        isLoadingTitles = true
        SampleServices.getTitles({ result in
            if let error = result.error {
                // TODO: improved error handling
                self.isLoadingTitles = false
                let alert = UIAlertController(title: "Error", message: "Could not load first titles :( \(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            let titlesWrapper = result.value
            self.addTitlesFromWrapper(titlesWrapper)
            self.isLoadingTitles = false
            self.tableview?.reloadData()
            
            self.activityIndicator.stopAnimating()
        })
    }
    
    func loadMoreTitles() {
        self.isLoadingTitles = true
        if let titles = self.titles,
            let wrapper = self.titlesWrapper,
            let totalTitlesCount = wrapper.titles?.count,
            titles.count < totalTitlesCount {
            //       there are more titles out there!
            SampleServices.getMoreTitles(titlesWrapper, completionHandler: { result in
                if let error = result.error {
                    self.isLoadingTitles = false
                    let alert = UIAlertController(title: "Error", message: "Could not load more titles :( \(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                let moreWrapper = result.value
                self.addTitlesFromWrapper(moreWrapper)
                self.isLoadingTitles = false
                self.tableview?.reloadData()
            })
        }
    }
    
    func addTitlesFromWrapper(_ wrapper: SampleWrapper?)
    {
        self.titlesWrapper = wrapper
        if self.titles == nil {
            self.titles = self.titlesWrapper?.titles
        } else if self.titlesWrapper != nil && self.titlesWrapper!.titles != nil {
            self.titles = self.titles! + self.titlesWrapper!.titles!
        }
    }
    
    // MARK: TableViewDataSource
    
    // delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            titles?.remove(at: indexPath.row)
//            saveMeals()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return self.titlesSearchResults?.count ?? 0
        } else {
            return self.titles?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableview!.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell
        
        var arrayOfTitles: [SampleServices]?
        if searchController.isActive {
            arrayOfTitles = self.titlesSearchResults
        } else {
            arrayOfTitles = self.titles
        }
        
        if arrayOfTitles != nil && arrayOfTitles!.count >= indexPath.row {
            let titles = arrayOfTitles![indexPath.row]
            cell.textLabel?.text = titles.title
            cell.detailTextLabel?.text = " " // if it's empty or nil it won't update correctly in iOS 8
            cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
            cell.imageView?.image = nil
            if let name = titles.thumbnailUrl{
                // check the cache first
                if let cachedImageResult = imageCache?[name] {
                    // TODO: custom cell with class assigned for custom view?
                    cell.imageView?.image = cachedImageResult?.image // will work fine even if image is nil
                    if let attribution = cachedImageResult?.fullAttribution(), attribution.isEmpty == false {
                        cell.detailTextLabel?.text = attribution
                    }
                } else {
                    
                    SearchController.imageFromSearchString(name, completionHandler: {
                        result in
                        if let error = result.error {
                            print(error)
                        }
                        // TODO: persist cache between runs
                        let imageSearchResult = result.value
                        self.imageCache![name] = imageSearchResult
                        if let cellToUpdate = self.tableview?.cellForRow(at: indexPath) {
                            if cellToUpdate.imageView?.image == nil {
                                cellToUpdate.imageView?.image = imageSearchResult?.image // will work fine even if image is nil
                                cellToUpdate.detailTextLabel?.text = imageSearchResult?.fullAttribution()
                                cellToUpdate.setNeedsLayout() // need to reload the view, which won't happen otherwise since this is in an async call
                            }
                        }
                    })
                }
            }
            
            if !searchController.isActive {
                // See if we need to load more titles
                let rowsToLoadFromBottom = 5;
                let rowsLoaded = self.titles!.count
                if (!self.isLoadingTitles && (indexPath.row >= (rowsLoaded - rowsToLoadFromBottom))) {
                    if let totalRows = self.titlesWrapper?.titles?.count {
                        let remainingTitlesToLoad = totalRows - rowsLoaded;
                        if (remainingTitlesToLoad > 0) {
                            self.loadMoreTitles()
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let titlesDetailVC = segue.destination as? TitlesDetailViewController {
            
            // gotta check if we're currently searching
            guard let indexPath = self.tableview?.indexPathForSelectedRow else {
                return
            }
            if searchController.isActive {
                titlesDetailVC.titles = self.titlesSearchResults?[indexPath.row]
            } else {
                titlesDetailVC.titles = self.titles?[indexPath.row]
            }
        }
    }
    
    // MARK: Search
    func filterContentForSearchText(_ searchText: String, scope: Int) {
        // Filter the array using the filter method
        if self.titles == nil {
            self.titlesSearchResults = nil
            return
        }
        self.titlesSearchResults = self.titles!.filter({(aTitles: SampleServices) -> Bool in
            // pick the field to search
            let fieldToSearch: String?
            
            //if you want to search multiple option
//            switch (scope) {
//                        case (0):
                          fieldToSearch = aTitles.title
//                        case (1):
//                            fieldToSearch = aTitles.id
//            default:
//                fieldToSearch = nil
//            }
            
            guard let field = fieldToSearch else {
                self.titlesSearchResults = nil
                return false
            }
            return field.lowercased().range(of: searchText.lowercased()) != nil
        })
    }
    
    // NEW
    func updateSearchResults(for searchController: UISearchController) {
        let selectedIndex = searchController.searchBar.selectedScopeButtonIndex
        let searchString = searchController.searchBar.text ?? ""
        filterContentForSearchText(searchString, scope: selectedIndex)
        tableview?.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let selectedIndex = searchBar.selectedScopeButtonIndex
        let searchString = searchBar.text ?? ""
        filterContentForSearchText(searchString, scope: selectedIndex)
        tableview?.reloadData()
    }
}


