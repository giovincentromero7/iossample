//
//  SampleServices.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 01/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation
import Alamofire

class SampleWrapper {
    var titles: [SampleServices]?
}

enum SampleFields: String {
    case albumId = "albumId"
    case id = "id"
    case title = "title"
    case url = "url"
    case thumbnailUrl = "thumbnailUrl"
    
}

class SampleServices {
    var albumId: String?
    var id: String?
    var title: String?
    var url: String?
    var thumbnailUrl: String?
    
    required init(json: [String: Any]) {
        
        self.albumId = json[SampleFields.albumId.rawValue] as? String
        self.id = json[SampleFields.id.rawValue] as? String
        self.title = json[SampleFields.title.rawValue] as? String
        self.url = json[SampleFields.url.rawValue] as? String
        self.thumbnailUrl = json[SampleFields.thumbnailUrl.rawValue] as? String
     
    }
    
    // MARK: Endpoints
    class func endpointForID(_ id: Int) -> String {
        return "https://jsonplaceholder.typicode.com/photos/\(id)"
    }
    class func endpointForTitles() -> String {
        return "https://jsonplaceholder.typicode.com/photos"
    }
    
    
    
    // MARK: CRUD
    // GET / Read single titles
    class func titlesByID(_ id: Int, completionHandler: @escaping (Result<SampleServices>) -> Void) {
        let _ = Alamofire.request(SampleServices.endpointForID(id))
            .responseJSON { response in
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                let titlesResult = SampleServices.titlesFromResponse(response)
                completionHandler(titlesResult)
        }
    }
    
    // GET / Read all titles
    fileprivate class func getTitlesAtPath(_ path: String, completionHandler: @escaping (Result<SampleWrapper>) -> Void) {
        // make sure it's HTTPS because sometimes the API gives us HTTP URLs
        guard var urlComponents = URLComponents(string: path) else {
//            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
//            completionHandler(.failure(error))
            return
        }
        urlComponents.scheme = "https"
        guard let url = try? urlComponents.asURL() else {
//            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
//            completionHandler(.failure(error))
            return
        }
        
        let _ = Alamofire.request(url)
            .responseJSON { response in
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                print(response)
                let titlesWrapperResult = SampleServices.titlesArrayFromResponse(response)
                completionHandler(titlesWrapperResult)
        }
    }
    
    class func getTitles(_ completionHandler: @escaping (Result<SampleWrapper>) -> Void) {
        getTitlesAtPath(SampleServices.endpointForTitles(), completionHandler: completionHandler)
    }
    
    class func getMoreTitles(_ wrapper: SampleWrapper?, completionHandler: @escaping (Result<SampleWrapper>) -> Void) {
        //    guard let nextURL = wrapper?.next else {
        //      let error = BackendError.objectSerialization(reason: "Did not get wrapper for more titles")
        //      completionHandler(.failure(error))
        //      return
        //    }
        getTitlesAtPath("1", completionHandler: completionHandler)
    }
    
    private class func titlesFromResponse(_ response: DataResponse<Any>) -> Result<SampleServices> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }
        
        // make sure we got JSON and it's a dictionary
        let json = response.result.value as? [String: Any]
//        else {
//            print("didn't get titles object as JSON from API")
//            return .failure(BackendError.objectSerialization(reason:
//                "Did not get JSON dictionary in response"))
//        }
        
        let titles = SampleServices(json: json!)
        return .success(titles)
    }
    
    private class func titlesArrayFromResponse(_ response: DataResponse<Any>) -> Result<SampleWrapper> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }
        
        // make sure we got JSON and it's a dictionary
//        guard let json = response.result.value as? [String: Any] else {
//            print("didn't get titles object as JSON from API")
//            return .failure(BackendError.objectSerialization(reason:
//                "Did not get JSON dictionary in response"))
//        }
        
        let wrapper:SampleWrapper = SampleWrapper()
        
       
        var allTitles: [SampleServices] = []
        
        if let results = response.result.value as? [[String: Any]] {
            for jsonTitle in results {
                let title = SampleServices(json: jsonTitle)
                allTitles.append(title)
            }
        }
        
        wrapper.titles = allTitles
        return .success(wrapper)
    }
    
    
}
