//
//  StringArrayConvertible.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 01/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation

extension String {
    func splitStringToArray() -> [String] {
        var outputArray = [String]()
        
        let components = self.components(separatedBy: ",")
        for component in components {
            let trimmedComponent = component.trimmingCharacters(in: CharacterSet.whitespaces)
            outputArray.append(trimmedComponent)
        }
        
        return outputArray
    }
}
