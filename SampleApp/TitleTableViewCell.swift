//
//  TitleTableViewCell.swift
//  SampleApp
//
//  Created by Gio Vincent Romero on 01/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit

class TitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
